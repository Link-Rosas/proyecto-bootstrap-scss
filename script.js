var app = new Vue({
    el: "#app",
    data: {
        mensaje: "Listado de Productos ",
        numero: 2022,

        objeto: [{
            producto: "Producto 1",
            cantidad: 0,
            precio: 0

        }, {
            producto: "Producto 2",
            cantidad: 8,
            precio: 1000

        }, {
            producto: "Producto 3",
            cantidad: 10,
            precio: 2000

        }, ],
        nuevoProducto: "",
        cantidad: 0,
        precio: 0,
        subtotal: 0,
        totalInventario: 0
    },
    methods: {
        agregarProducto() {
            this.objeto.push({
                producto: this.nuevoProducto,
                cantidad: Number(this.cantidad),
                precio: Number(this.precio),
            })
        },
        eliminarProducto() {
            this.objeto.pop()
        }
    },
    computed: {

        calculartotalInventario() {
            this.totalInventario = 0;
            for (item of this.objeto) {
                subtotal = item.cantidad * item.precio;
                this.totalInventario = this.totalInventario + subtotal;
            }
            return this.totalInventario;
        }
    }
})